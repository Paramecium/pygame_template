import pygame
import sys
from math import pi, sin, cos
import random

class Ball():
    def __init__(self, source, x, y, speed = 10):
        self.source = source
        ball = pygame.image.load(source)
        self.obj = ball
        self.rect = ball.get_rect()
        self.rect.x = x
        self.rect.y = y
        self.speed = speed


gameover = False
color = (255, 255, 0)
size = width, height = 800, 600
speed = 10
ball = Ball("basketball.png", 400, 300, 10)
angle = random.uniform(0, 2 * pi)
screen = None
bg = None

def handle_events():
    global gameover
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            gameover = True



def init():
    global screen, bg
    pygame.init()
    bg = (0, 0, 0)
    screen = pygame.display.set_mode(size)


def main():
    global gameover, radius, size, speed, angle, wid, hei, color, ball
    init()
    while not gameover:
        handle_events()
        screen.fill(bg)
        if ball.rect.x <= 0 and cos(angle) < 0:
            angle = pi - angle
        if ball.rect.x + ball.rect.width >= width and cos(angle) > 0:
            angle = pi - angle
        if ball.rect.y + ball.rect.height >= height and sin(angle) > 0:
            angle = 2 * pi - angle
        if ball.rect.y  <= 0 and sin(angle) < 0:
            angle = 2 * pi - angle
        ball.rect.x += int(cos(angle) * ball.speed)
        ball.rect.y += int(sin(angle) * ball.speed)
        screen.blit(ball.obj, ball.rect)
        pygame.display.flip()
        pygame.time.wait(10)
    sys.exit()


main()